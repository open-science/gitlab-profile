**Git setup for coding researchers**

[Back to home](README.md)

[[_TOC_]]


# Every new coding project

To do: Installing Git on your specific coding project folder, and connect this local git repository to the remote project repository at git.wur.nl. We assume that the computer iself has already been connected, as described in the second part of this page ("Every new computer").

## Local set-up

Within Git a new project folder has to be initialised. First **start up Git bash** (= Git command line) in the right folder: 

![Screenshot starting up GIT bash](GIT_start_up.png "")


Then, **initialise** the folder with `git init --initial-branch=main`

![Screenshot starting up GIT bash](GIT_init.png "")


## Remote set-up

Now go to the webinterface of the remote, (https://git.wur.nl). You can login with your WUR credentials. Then, create a blank project:

![Screenshot create blank remote project](GIT_create_blank_remote_project.png "")


## Connect local to remote

From the next page, you can use a part of the code given by "Push an existing folder". Make sure there is at least one file already in your local folder (otherwise, execute `touch README.md`).  Then, you can copy-and-paste this code from the website to your local bash:

![Screenshot add remote, first push](GIT_remote_add.png "")

Your local output should look like:

![Screenshot add remote, local output](GIT_remote_add_local_output.png "")

And when you refresh the webpage of the remote project, you will notice that you indeed pushed a file from local to remote:

![Screenshot add remote, remote result](GIT_remote_add_remote_result.png "")


Conclusion: Now you have created your local and your remote project repositories, and connected those two!

# Every new computer 

To do: Installing Git, and connect your device to git.wur.nl


*Check 1*: can you connect to the remote server, are the SSH keys valid?

`ssh -T git@git.wur.nl`

*Check 2*: Is the local repository also mirrored remote?

`git ls-remote`


## Install Git

  - WUR employees: you can install GIT using the software center. Just select Git `version number`
  - Students: download and install from (https://git-scm.com/downloads) or (https://gitforwindows.org/)
  
Note that there are also Graphical User Interface (GUI) skins for Git, such a TortoiseGit and the build-in GIT GUI. In my experience, these are less intuitive than the local command line. Therefore, for this manual, I focus on the command line for local actions and the webinterface for remote actions on git.wur.nl. To facilitate also the non-R users, I did not take into account the build-in Git options in RStudio.

## Connecting the installation

About connecting: see [WUR GIT helpfiles](https://library.wur.nl/WebQuery/edepot/635269), and note that you have to do this only once (so you can also ask somebody else do it it for you).  An often used term is 'ssh keys', roughly (and not entirely correct) you can consider this as a locally stored password, so that you can smoothly exchange information to and from your computer (=local) to git.wur.nl (=remote), without the need to authenticate yourself with every action. This ssh key is locally generated and stored, and a complementary part of it is put remote using the webinterface of git.wur.nl. 
I will add a description with screenshots here as soon as I have to connect a new device myself. (latest update on this section: June 21, 2024 :innocent:  )




