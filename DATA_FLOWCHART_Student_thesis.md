**Master thesis: student and supervisor**

[Back to home](README.md)

![Student thesis](DATA_FLOWCHART_Student_thesis.png "")


Notes: 

1) When we say *sharing*, we mean it in the Microsoft 365 way: so granding others live access, in most cases read and write, to certain working documents. This is for everybody *much much* easier than ping-ponging static documents via email.

1) The proposed situation is that the students share their folder with their supervisors, but the other way round is also possible!


[Detailed start instructions](DMP_StartAsMScThesisStudent.md)

[Source file (Microsoft Visio)](DATA_FLOWCHART_Student_thesis.vsxd)