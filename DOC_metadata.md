**Structured metadata**

[Back to home](README.md)

## What is it?

With **structured metadata, the focus is on machine readable information** about your dataset, unlike a human directed [README.txt](DOC_thoughts_about_readme.md). Often, it are the webform fields you need to fill in when uploading a dataset into a repository.

The **long term vision** about open data is to have all data machine searchable and machine accessible. In other words: you, as researcher, doesn't have anymore to look out for different datasets, download each seperatly, and glue those together by hand; rather you will be able to formulate a research question: which kind of datasets you would like to include, and how they should be processed. The algorithm should do the rest, while still be completely transparant and reproducible (unlike AI approaches). Supplying each dataset with structured metadata is an essential step towards this long term vision.

## How to create and use it?

When you [upload a dataset to a repository :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/finishing/publishing-research-data.htm) yourself, you already have to fill in a form. This is the structured metadata with this dataset. Often it is based on the general purpose "Dublin Core".  There are sometimes field-specific structured metadata standards.

For an archived dataset, or a dataset in use which will be published or archived in the future, it is a good habit to add structured metadata already. You can use the [Yoda editor :link:](https://utrechtuniversity.github.io/yoda-portal/) or [CodeMeta generator (focussed on software) :link:](https://codemeta.github.io/codemeta-generator/), and download the metadata as .json data file. You can later upload such a file for editing. 

Note that the file system [Yoda](YODA_Basics.md), promoted at WUR and other Dutch institutions, is basically designed around a build-in structured metadata editor; also with above long term vision in mind. 

Because there is quite some **overlap** between the information in **structured metadata**, and in the **README** file, one might ask if there is a tool which converts a structured metadata file into a draft README. This would save time and lower the probability on errors. I could not find such a tool, but I am happy with suggestions!
