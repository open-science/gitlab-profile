**Open science at Soil Science Cluster**

[[_TOC_]]

*This page is maintained by WUR data steward [Luc Steinbuch](https://www.wur.nl/en/persons/luc-steinbuch-1.htm):link:, and is intended as an overview and information lookup point for all scientists (scientific staff, PhD candidates and master thesis students) as well as those teaching-related within the [Soil Science Cluster](https://edu.nl/6fd7b):link: at Wageningen UR. Several topics are under construction; latest update: February 2025*

General Introduction: [Open science ambitions of WUR](https://www.wur.nl/en/about-wur/our-values/finding-answers-together/open-science-education.htm):link: and the [Open Science Community Wageningen](https://openscience-wageningen.com/):link:.

## Research data management roles as flowchart

-   [Master thesis: student and supervisor](DATA_FLOWCHART_Student_thesis.md)
-   [General research project: PI or research project support](DATA_FLOWCHART_General_research.md)


## Research data management

-   Collection of useful links about [research data management at WUR](https://www.wur.eu/rdm):link:; the [latest WUR templates](https://zenodo.org/communities/wur-rdm-support/records?q=&l=list&p=1&s=10&sort=newest):link:
-   [Start your Data Management Plan as Soil Cluster PhD Candidate](DMP_StartAsPhD_Candidate.md)
-   [Start your Data Management Plan as Soil Cluster MSc thesis student](DMP_StartAsMScThesisStudent.md)
-   [Supervise a Soil Cluster MSc thesis](DMP_SuperviseMScThesisStudent.md)
-   [Finishing your MSc thesis without having written a DMP](DATA_finishing_your_MSc_thesis.md)
-   [Thoughts about README.txt](DOC_thoughts_about_readme.md)
-   [Structured metadata](DOC_metadata.md)
-   [Access to W: drive from outside the Campus or for students who bring their own device :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010660)
-   Access to WUR Onedrive for Business for students who bring their own device, in [English :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010699#OneDrive) and in [Dutch :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010698#OneDrive)
-   [Yoda very easily explained](YODA_Basics.md)
-   Backup-application (for example from Onedrive to W:), including scheduling: See **Syncback** in the WUR Software Center, or download it yourself.
-   [Research data storage](STORAGE_general.md)
-   Nice open workshops at [Data Carpentries](https://datacarpentry.org/):link: and [Library / DMP Carpentries](https://librarycarpentry.org/):link:
-   Consider [writing a data paper](DATA_data_paper.md)
-   [Publish your dataset](DATA_publish_dataset.md)
-   [Why -- and how to -- register a non-public dataset?](PURE_why_how_unpublished_dataset.md)

## Open education

-   [Open education at WUR](https://www.wur.nl/en/about-wur/our-values/finding-answers-together/open-science-education/open-education.htm):link:
-   [Library for Learning](https://library.wur.nl/WebQuery/l4l/find?status=*&wq_max=50&wq_srt_desc=ijaar):link:

## Open publishing

-   [WUR Library journal browser](https://library.wur.nl/WebQuery/wurbrowser?q=*):link:

## How to keep yourself motivated

-   [Intrinsic motivation](IntrinsicMotivation.md)
-   [Institutional motivation](InstitutionalMotivation.md)

## Register everything in the Research Information System (RIS/PURE/ [research.wur.nl](https://research.wur.nl))

-   [Why -- and how to -- register a non-public dataset?](PURE_why_how_unpublished_dataset.md)
-   [Connect PURE to ORCID :link:](https://edepot.wur.nl/550634)

## Privacy and safety

# Coding and computing

(not always directly related to Open Science indeed, but also important!)

## Installing R and R studio

WUR colleague [Maikel Verouden](https://www.wur.nl/nl/personen/maikel-verouden-1.htm):link: created and maintains several nice manuals about:

-   [Installing R and R Studio on different systems](https://www.verouden.net/category/r/):link:
-   [Installing Anaconda Python on different systems](https://www.verouden.net/category/anaconda/):link:

"Different systems" means several Operating Systems (Windows, Apple and Linux) but also WUR and non-WUR laptops. If you are a student and need to bring your own Windows laptop, and need R and R Studio: please have a look at [install R](https://www.verouden.net/post/2020/04/06/r-installation-windows-10/):link: and then [install RStudio](https://www.verouden.net/post/2020/04/13/rstudio-installation-on-windows-10/):link:; if you are a WUR employee (including PhD candidate) and need R and R Studio on your WUR laptop, have a look at [install R](https://www.verouden.net/post/2021/01/24/r-installation-wurclient/):link: and [install RStudio](https://www.verouden.net/post/2021/01/24/r-installation-wurclient/):link:.

## Good coding practices

-   [Git basics for coding researchers](GIT_BasicsForCodingResearchers.md); [Git setup](GIT_SetupForCodingResearchers.md)

-   Nice workshops at [Software Carpentries](https://software-carpentry.org/lessons/):link:, see for example [Writing good software](https://swcarpentry.github.io/r-novice-gapminder/15-wrap-up.html):link:. Even better, Follow a [course at the Netherlands eScience Center](https://www.esciencecenter.nl/digital-skills/):link: or at the [WUR library](https://www.wur.nl/en/library/researchers/courses-and-demos-staff/introduction-to-gitlab.htm):link:

-   [Software Licences](https://en.wikipedia.org/wiki/Software_license):link:; I often use "MIT".

## Using the WUR HPC (High Performance Computing)

-   [Open on demand & R Studio](HPC_open_on_demand.md)

-   [About the HPC](https://search.researchequipment.wur.nl/SearchDetail.aspx?deviceid=8d495133-99ee-4b81-b059-bbef6d6734b8):link:, [description on the support pages](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010705&sys_kb_id=5b74299183affd186d06f665eeaad358&spa=1):link:, the [Wiki](https://wiki.anunna.wur.nl/index.php/Main_Page):link:, and [how to get access](https://support.wur.nl/esc?id=sc_cat_item&table=sc_cat_item&sys_id=e52c5a188776a150b0bd33fd3fbb3536):link:.

-   WUR HPC SLURM Trainings in 2025: [Linux base](https://www.wur.nl/en/activity/linux-basic-course.htm):link:, [HPC Basic](https://www.wur.nl/en/activity/hpc-basic-course.htm):link: and [HPC Advanced](https://www.wur.nl/en/activity/hpc-advanced-course.htm):link:




