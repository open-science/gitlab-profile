**Publish your dataset**

[Back to home](README.md)

[[_TOC_]]

\>\> See also a [nice clickable infographic created by WUR-WDCC :link:](https://www.lucsteinbuch.nl/wur_data_sharing_guidelines_infographic_20231205_v02-1.pdf) about when you can publish a dataset. \<\<

You can [ask the WUR library to do it for you :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/finishing/publishing-research-data.htm), or do it yourself. In the latter case, my personal guidelines:

1) If your "dataset" is **data in the sense of "collected observations" or "computer code"**, or a combination of both: use [4TU.ResearchData :link:](https://data.4tu.nl/). It offers version control (a fixed DOI to the latest version of a dataset, and separate DOI's to earlier versions), for the coders git integration, and you are sure that your meta data is reviewed -- also by WUR library staff -- and, if approved, quickly appears in PURE. Your data size limit is 100GB/researcher/year.

2) If you have a **more liquid dataset** to share, use [Zenodo :link:](https://zenodo.org/). For example, WUR library puts its [MSc DMP templates :link:](https://doi.org/10.5281/zenodo.12805038) on Zenodo; I myself put a an [ACT report with deliverables :link:](https://doi.org/10.5281/zenodo.14621760). The data size limit is 50GB/deposit.

3) Often, some repositories have a **specific target group**. If you are working closely together with [NIOO :link:](https://nioo.knaw.nl/en/who-we-are), perhaps the ecology origins of [Dryad :link:](https://datadryad.org/) make this the obvious choice; for bioinformatics sequencing data, [European Nucleotide Archive :link:](https://www.ebi.ac.uk/ena/browser/home) is one of th emany options. If you happen to work rather on the humanities and social side of sciences, or archeology, one of the four [DANS/KNAW Data Stations :link:](https://dans.knaw.nl/en/data-stations/) might be the most logical choice, despite it's less intuitive interface and the difficult to find [manual :link:](https://dans.knaw.nl/en/depositing-data-manual/during-depositing_ds/). An overview of repositories is for example provided by [Fairsharing :link:](https://fairsharing.org/search?page=6&recordType=repository&isMaintained=true&isRecommended=true&status=ready) and [re3data :link:](https://www.re3data.org/browse/by-subject/).


Note that with many repositories one can **get a DOI before publishing actually data** (so one can crosslink with DOI's between data <-> peer reviewed publication). Many data repositories can also be used to store your dataset, while the **data itself is not publicly shared** temporarily ("under embargo") or not at all ("restricted"). 

Always **make sure that WUR is mentioned** as one of the affiliated organisations, and that your dataset is also registered in PURE, by sending its DOI to *data DOT library AT wur.nl*. 

