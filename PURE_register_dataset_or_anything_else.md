**Register dataset or anything else at PURE/research.wur.nl**

[Back to home](README.md)


[research@wur.nl :link:](https://www.wur.nl/en/library/research-at-wur.htm) ([research.wur.nl :link:](https://research.wur.nl); internally also called PURE or RIS: Research Information System) is an internal system at WUR to show your scientific portfolio in a broad sense to the rest of the world. It also allows (but not obligates) to upload and share the scientific output itself. This scientific output includes: media performances, presentations, presentation materials, abstracts, published datasets, [archived datasets](PURE_why_how_unpublished_dataset.md), etc. apart from the usual peer reviewed papers. Main point of contact: *pure DOT library AT wur DOT nl*.


To add your output, send the [needed information :link:](https://intranet.wur.nl/Project/ResearchInformationIntelligenceServices/Pages/P8OnwvZFdEKa2EcLwWMq7w) to your SSC datasteward or to *pure DOT library AT wur DOT nl*; if you want to do it yourself you can give it a try at [ris.wur.nl](https://ris.wur.nl). 

  - [**Internally archived datasets**](PURE_why_how_unpublished_dataset.md)

  - **Published datasets, and non-published datasets archived in data repositories**: There are some automated systems in place (assuming that *one of the affiliations is something WUR related*), but the PURE team is happy for a heads-up every time a dataset is deposited. This is not needed for depositions in [4TU research data :link:](https://data.4tu.nl/) (because the meta-data is reviewed anyway), and also not needed when the [WUR library does depositing your data in 4TU, DANS or Zenodo :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/finishing/publishing-research-data.htm).
  
  - **Review activities**: At the moment of writing (February 2025), WUR PURE does offer the possibility to add review activities but does not actively encourage its use; unlike being editor, there is also no internal reporting on being reviewer. Systems like [Web of Science :link:](https://webofscience.com) are better equipped for this.
  
  - **Peer reviewd papers**: for almost all journals, these will be added automatically if your affiliation is also something WUR related. If you want to have your pre-WUR publications also added, contact the library.


Review actions



### How to register a not-published dataset?


1. Collect the following information: 
 * Name of dataset
 * Location of dataset
 * Creators etc (if possible with ORCID)
 * Contact person
 * Related publication or publications
 * Terms of use, license.
2. Create a frozen and well-[documented](DOC_thoughts_about_readme.md) version of your dataset. This should also contain the information collected in the previous point.  
3. Put this frozen dataset at a logical place, such as the W:drive. Other options are the WUR or SURF tape archive, or YODA. 
4. Send all information, with a copy of the README.txt (or something similar), to your data steward, or to data AT wur.nl. 

Thanks! Your future self (among others) will be grateful for your actions!

