**Finish your MSc thesis: ordering your data**

[Back to home](README.md)

[[_TOC_]]

## Goal of this page

Inside WUR, and inside the Soil Science Cluster chairgroups (SBL, SGL, SLM and SOC), we are working towards a situation where each Master thesis student writes a Data Management Plan (DMP) alongside the research proposal. However, as we are not quite there yet, this page may help you to round off your thesis in terms of research data without a DMP.

## Intention: data at WUR

As WUR, we strive to store all data underlaying research properly and well-documented, and also centrally registered, because:

-   In case of an MSc thesis, the data must be kept for at least 10 years for reasons of ‘reproducibility’.

-   For science in general, we strive to make datasets "as open as possible", so they can be re-used: saving time and effort for other researchers. See [WUR Open Science & Education :link: ](https://www.wur.nl/en/about-wur/our-values/finding-answers-together/open-science-education.htm) for the formal background.

## Basic question & requirements

*Can somebody else, or yourself over one year* (as you will forget faster than you realise now!), *understand which data is were, what it is about, how to reproduce your research, and if any part of it can be re-used?* One should be able to get an overview without having to read the thesis itself. For the details, however, you can of course refer to the thesis: we should try to avoid doing the same work twice.

Main requirements are:

1.  Clear, logical and complete folder structure

2.  Logical file names

3.  If possible, all data also in a non-exotic file format

And, there should be a "`README.txt`" file in the top folder, which explains:

1.  Organisational context: which thesis does the data connect to, who created it, etc.?

2.  Which part of the data comes from other sources (and: which other sources?), which part are the raw observations by you -- the student --, where is the processed data and the output?

3.  What does the data exactly mean? Units, laboratory protocols etc.

4.  Which part of the data can be shared with others (and thus re-used), and: under which conditions?

5.  If there is computer code: this should be included and explained as well.

A template & example of such a `README.txt` file:

```         
Data belonging with MSc thesis "<< the name of your thesis >>"

## Related MSc thesis
"<< the name of your thesis >>", << your name >>, << the chairgroup >>,  Wageningen University, August 2024.  Supervisor: << name supervisor >> 

## Topic summary
<< Copy-and- paste from the abstract of your thesis >>

## Folderstructure << This is just a suggestion, there are of course many many possibilities! You can stay close to the structure you are already using now >>
- RawData  
-- LabResults
-- FieldObservations
-- ImportedCovariates
- ProcessedData
-- Tables
-- Figures
-- Maps
- Code
- Admin
-- Report
-- ResearchProposal
-- Literature
-- Meetings

## Files; software
<< Which kind of files are there; which software was used? If you wrote your own code: did you need to install any special libraries? >>

## Origin of re-used data
<< In case you downloaded data, or got data from your supervisor: explain were it comes from. Perhaps there is a table in your thesis with all this information? In that case, just refer to this table >>

## Codebook
<< Meaning of data: units, geographical projection, way of aquiring, used laboratury analysis etc. If this is clearly described in the "Methods" part of your thesis, you can refer to the appropriate section >>

## Used workflow
<< With that datafile, do this, to arrive at that datafile. Probably a bit more hands-on than in your "Methods" section>>

## License; terms of use
This data set is internally archived at WUR for reproducibility purposes, and as such not meant to be made public or to be shared. However, the files << your own observations, and / or your own code >> can be re-used under CC-BY license << if you want this; "CC-BY" means that others can use it but should cite you as the author >>
```

For inspiration, see [more about a README file](DOC_thoughts_about_readme.md); however some parts might not be relevant for you.

## Finalising your dataset

When you are finished with organising the data and the `README.txt` file, you can share it – for example via Onedrive or via the [surf filesender :link:](https://filesender.surf.nl/) – with your supervisor and with your data steward (Luc Steinbuch) for a final check. If you happen to have access yourself to the long term storage facility, for example the "WUR W: drive", you can then upload it yourself; otherwise we will do it for you.

As it is my function to facilitate you, I am happy to answer your questions!

🙂 Luc (Steinbuch)
