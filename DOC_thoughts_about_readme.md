**Thoughts about a README file**

[Back to home](README.md)

## What is it?

A "README" file is the standard way to provide information about your dataset in a human-readable format. It gives an overview of contents and context of your data or codeset to somebody else, and also -- most important -- **an easily accessible overview for yourself in the future**. This will save a lot of time (for example when you get the reviewers' report after many months). Usually a README is a plain text file (so "README.txt") but a pdf is also acceptable (more possibilities, but more work to update). When you are working with code in a git* environment, you are strongly invited (almost forced) to create a README.md, which describes what your codeset does in [markdown :link:](https://en.wikipedia.org/wiki/Markdown) format. Note that when you search the Internet for "how to write a readme?", most results relate to a README for code, rather than one for datasets.

## How to create and use it?

A README file should be placed in the root of your dataset; if it adds to clearity, certain directories (such as a specific dataset, or the directory with programming code) can have their own specific README. Unlike forms with "metadata", who have a similar function as a README but are rather focussed on being machine-readable, you have quite some freedom how to write a README. Templates, usable for data, code, or a combination of both : [easy template by Luc and Cindy](README_template.txt); or [more formal by WUR :link:](https://doi.org/10.5281/zenodo.7701727); or a [very basic one for MSc student theses](README_MSc_template.txt).

A README can include (please make your choice in what is relevant for your data/codeset; the topics in bold will always be relevant):

  - **Title** 
  - Introduction & context
    - Goal of the dataset/project; short description; abstract
    - **Institution/authors/creators/contact**\
    \>\> An [ORCID :link:](https://orcid.org) would be helpful. Perhaps be careful with email to avoid spam.
    - Date, update number, versioning
    - Space and time context
    - Connection to paper, data paper, other research etc.
    - DOI/URL of codeset itself
    - Funding, contributors, acknowledgements
    - Keywords
    - **[License :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/finishing/licences.htm) / restrictions on sharing**\
    \>\> Sometimes, the license is a link or a separate document
  - File structure & conventions
    - File & directory overview
    - File formats, if not obvious
    - Naming conventions
  - Code description
    - Programming language
    - Used libraries, dependencies, version\
    \>\> For R users: `sessionInfo()` provides an overview of all relevant information
    - How to replicate workflow, or to use software
    - Expected calculation time; used hardware
    - How is the algorithm tested/ verified/ validated? 
    - Link to maintained version (for example on git.wur.nl) and to frozen version (for example on data.4tu.nl, indicated with a doi)
  - Data description\
  \>\> Repeat for every data file; you might refer to a paper or report 
    - How is the data collected, or analysed?
    - How is the data tested/ verified/ validated?
    - Indication of uncertainty
    - References to related testing information
    - Required software to read the files
    - For tabular data: column headings, and their meaning\
    \>\> Sometimes this is in a separate "codebook" or "data dictionary"
    - Units of measure; data notation; geographical projection etc.
    - Are data obscured / anonimized for privacy?
    


---


Contentwise based on Module 3 of Quik C, Steinbuch L. 2022. Materials from: Workshop FAIR data and data reuse for Environmental Science Group researchers. 4TU.Centre for Research Data. DOI: [10.4121/21399975 :link:](https://doi.org/10.4121/21399975)  

Available under CC-BY-NC; Luc Steinbuch, Wageningen UR, 2024






