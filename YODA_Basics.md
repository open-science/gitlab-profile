**Yoda very easily explained**

[Back to home](README.md)

[[_TOC_]]

# What is YODA?

Yoda is a research data management application under development by several Dutch universities & SURF, started by Utrecht University. Its goal is to help you managing and saving research data in a professional way; Yoda will help researchers work according to FAIR principles, for example by explicitly demanding metadata, and offering the possibility of creating a frozen snap-shot of the data at any moment in time ("the vault"). Note that at the moment of writing (August 2024), it is yet not possible to publish datasets from the WUR Yoda instance. And that Yoda is suitable as backup and sequential sharing environment (inside WUR), not as daily working environment; nor does it allow real-time sharing.

# Getting access

Each Soil Science Cluster chairgroup has one or several data managers, one of those being your data steward. You can ask him/her/them to add your project, and thus to grant you access to the project files. You can, in turn, add others (such as your PhD supervisors).

# Use webinterface

Log in with your WUR credentials at <https://yoda.wur.nl>; then go to "Research" :arrow_right: your project.

![](YODA_research_weboptions.png)

Inside your project file system, you can create subdirectories, upload or download files etc. And, very important: **fill in the metadata form**; at least for the root of your project files, but eventually for every individual subfolder. This results in standardised information about your dataset, which can consistently be harvested by current and future dataset search engines.

![](YODA_metadata_form.png)

# Yoda folder on your computer

For easy access to files in Yoda (but not for adding or updating the metadata), you can make the yoda file system a network drive. See [the manual](https://servicedesk.surf.nl/wiki/display/WIKI/Connecting+to+Yoda+via+network+drive); the server address is `https://data.yoda.wur.nl` . For a regular automated backup, you can use scheduled backup software such as Syncback, which is in your WUR Software center.

Note that this connection via a network drive is slow (especially with many file manipulations); other options are mentioned in [this overview](https://www.wur.nl/nl/en/waardecreatie-samenwerking/wdcc-2/yoda-at-wur.htm); the manuals for the different options are given by the [files with the WUR YODA team channel](https://wageningenur4.sharepoint.com/:f:/r/sites/Yoda_Users/Gedeelde%20documenten/Yoda%20manuals?csf=1&web=1&e=1DHbUM). 
