>> The following is meant as a start-up help, and by no means complete nor obligatory <<
>> Remove everything you don’t need, and save it as a .txt., .md or .pdf <<
>> This template is part of https://doi.org/10.4121/21399975 and as template licensed under CC-BY-NC 4.0 <<
>> As finished README, all rights are waved <<
>> Created by Cindy Quik & Luc Steinbuch, Wageningen UR, 2022 <<


# <NAME DATASET AND/OR CODE PROJECT>
<Add here short summary of the data/code project, space & time context etc.>

## Creators, institution, contact
<Name, institution, ORCID, perhaps email>

## Contributers, founding

## Related sources
This dataset underlays the paper/report <..., DOI: ..>

## Online repository
This dataset/codeset is stored at <DOI/URL>

## Keywords

## License
The data is licensed under CC-BY-4.0
The software in this project is licensed under GPLv3

## Files
### Directories
### Filename conventions
### Used file formats

## Code project description 
### Programming language; used libraries, dependencies
### How to replicate workflow
### Expected calculation time; used operation system and hardware
### How tested & validated
### Active version / Frozen version
The maintained version of this project is at https://git.wur.nl/... 
<OR>
The frozen version of this  project at date ... is at <https://doi.org/...>

## Data description 
### Collection methodology
### Testing, validation
### Uncertainty
### Required software
### Column headings; units of measure; date notation; geographical projection etc.
### Obscured/anonimized data because of privacy
