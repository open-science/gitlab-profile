**Git basics for coding researchers**

[Back to home](README.md)

[[_TOC_]]


## I aim this webpage...

...to programming researchers, including students, at the Soil Science Cluster in Wageningen UR who are not familiar with automated version control and command-lines and having little interest in acquiring these skills *an sich*, but would like more structure and history keeping in their scientific programming.

## Why would you use Git?
Because using version control will make you a **better scientific coder**. It **helps to structure your file system**,  makes your **changes explicit, tractable and retreatable**, and -- if desired -- facilitates sharing your code during and after development. It is considered one of the good coding practices. Not that Git is developed for code and plain text documents, but less suitable for other types of data. By the way: "Git" might be considered as an abbreviation for "global information tracker" but its name has also [other origins](https://about.gitlab.com/blog/2022/04/07/celebrating-17-years-of-git/).

# Set-up

  1. **Installing GIT on your computer, and connect your computer to git.wur.nl**\
  Perhaps ask somebody else, such as [your data steward](https://www.wur.nl/en/persons/luc-steinbuch-1.htm). If you are not familiar with command-lines and generating ssh-keys, it is quite an effort which is needed only once. Check the Git [setup for coding researchers](GIT_SetupForCodingResearchers.md) if you still want to try.  

  2. **Installing GIT on your specific coding project folder, and connect this local git repository to the remote project repository at git.wur.nl**\
  Also here: if it is the first time, ask somebody else like [me](https://www.wur.nl/en/persons/luc-steinbuch-1.htm). You want to code and thereby to use Git as a tool, not get lost in many options and new terms. If if you are going to use Git for more coding projects, it would make sense to visit [Git setup for coding researchers](GIT_SetupForCodingResearchers.md). 



# Starting up GIT for one session, in one project

In the Explorer, select the project directory where your source code is, use your right-mouse button for the context dependent options and choose "Open Git Bash here":

![Screenshot starting up GIT bash](GIT_start_up.png "")


# Workflow with one user

You can keep nice track of what you are doing, and why:


![GIT workflow with one user](GIT_one_user.png "")

---

Intermezzo:  :thinking_face: What is a *useful description* to commit with?
 
 1. **Focus on what and why**, rather than on the *how*. In other words: describe the effect of the change, and the context.  Note that one can already display the actual change in the computer code, and thus how the change was implemented.
 
 2. **Start with a verb** as giving a command in the present tense, and add context. Examples:\
 “Remove dependency on outdated package rgdal”\
 “Split data loading function to increase robustness”\
 “Update documentation with installation instructions”\
 “Speed up data aggregation by parallel processing”\
 “Make output user friendlier because of stakeholder complains”\
 “Resolve divide-by-zero-elevation bug”\
 “Add cow age histogram because reviewers’ request”\
 It might be counterintuitive to use this “imperative mood” tense for the verb (because it is something you just did, so a past tense sounds more logical), but formulating your actions as if you are performing those in the present makes the list of commits easier readable.
 
 3. Use **clear and concise language**; avoid filler words (*just*, *like*, *totally* etc.).  If you can leave out a word without changing the meaning of the sentence, it's a filler word! 

---


## :fireworks: Your reward : project coding information on [git.wur.nl](https://git.wur.nl) 

First log in (institutional credentials). In the project overview, choose your project:

![GIT remote choose project](GIT_remote_choose_project.png "")

You can see **all files and the latest commits** regarding those files:

![GIT remote overview project](GIT_remote_overview_project.png "")

When you click on a commit, you can see **what actually changed in the code**:

![GIT remote overview commit](GIT_remote_overview_commit.png "")

And you can also **browse the old code** at the moment of earlier commits:

![GIT remote overview previous commits](GIT_remote_overview_previous_commits.png "")



# Workflow with two users, with good coordination and plenty of time

... which bij the way could also be one user -- you -- using two devices. We assume that those two users or devices never have to work simultaniously. This "lazy approach" workflow is like this:


![GIT remote overview previous commits](GIT_workflow_two_lowkey.png "")



# Workflow with two users working simultaniously

For serious coding, we can have more than just the "main" branch. Let's **create a second branch**, via the webinterface:

![GIT create new branch webinterface](GIT_create_new_branch.png "")

Note that we can now choose from two branches, both of which are available everywhere to browse (git.wur.nl) but also to work on (locally):

![GIT show two branches](GIT_show_two_branches.png "")

The complete workflow might look like this:

![GIT workflow two branches](GIT_workflow_two_more_complex.png "")

To merge those two branches back into one, we have to create a merge request and then approve it. (Those two steps for one action seems a bit overdone, but please remember that Git was developped with many coders in mind, where some coders can request a merge and the main coder can approve or deny this request.)

![GIT merge webinterface](GIT_workflow_merge.png "")




Please note that in this example, we did the branching and merging actions using the webinterface on the remote server. Like basically everything Git-related, it is also possible to do this via the command line, locally. 


:smiley: Happy coding!


---

License: CC-BY, Luc Steinbuch, Wageningen UR, Summer 2024

