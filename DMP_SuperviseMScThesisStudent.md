**Data Management Plan: supervise a Soil Science Cluster Master thesis student regarding data management**

[Back to home](README.md)

[[_TOC_]]


_Please note that the following instructions are in development, and might not be completely coherent with both the chairgroup Master Thesis Course Guide and the Data Management Protocol. Feel free to provide any feedback, and ask your questions, to your [Soil Science Cluster datasteward :link:](https://www.wur.nl/en/persons/luc-steinbuch-1.htm)!_

# Start thesis

1. **Inform students about the [Instructions](DMP_StartAsMScThesisStudent.md)** (and have a look at it yourself). Note that staff researchers, PhD's etc. are strongly recommended to use the online DMP system [dmp.wur.nl :link:](https://dmp.wur.nl), but the WUR student template is currently only available in MS-Word format.
2. Check that the student **indeed shares** his/her/their thesis work with you, via Onedrive or via a regular backup on the W:drive. This also to ensure that the data doesn't get lost when for example a laptop is stolen.
3. Have -- together with the data steward -- a look at the thesis DMP and **provide feedback** if needed. You can focus on the research-related aspects, but also on licencing issues if you want to use the data in follow-up research. Kindly remember that a DMP is meant to guide students towards proper data management; it's about intentions and how to implement those intentions smoothly into daily practice, not as a set of iron rules. 


# Finish thesis general

1. When the student is finishing up the thesis work, **check the dataset** (= filenames, directory structure) + documentation such as ```README.txt```: if you would need anything in a few years from now, is it clear to you where to find the information and eventual code you are looking for, how to use it, and if you are allowed to use it (=licenses)? 
2. Note that the question if the student literally followed the DMP is not important, but rather if one can re-use the dataset, for verification or for other research.
3. It is advisable to finalise this step before the examination, or at least before giving the grades.


## Finish thesis: internal archived dataset

4. **Make sure the codeset is at the correct location** on the W: drive (this differs per chairgroup) where the secretary of your group can grant a colleague access, even if you are no longer employed at WUR. Note that for very large datasets, the WUR tape drive might be a better option.
5. **Inform the WUR Research Information System** (for the outside world visible as [research.wur.nl :link:](https://research.wur.nl)) about the existence of the dataset, via pure.library AT wur.nl (and please CC the data steward). They need the name of the dataset, the information about the thesis it is connected to, the contact person (you), the name or names of the creator (the student, or the student and you) and some kind of abstract (which could be the abstract from the thesis). Note that this registration also adds to your scientific credits.

## Finish thesis: public dataset

If possible, and if you think the data is of high enough quality, we should aim for publishing the data. If the complete dataset is published, there is no need for internal archiving. It is however also possible to do both, or to just to publish a part. 

4. **Upload, or supervise the thesis student in uploading**, the dataset; see [Publishing research data – WUR Library supported repositories :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/finishing/publishing-research-data.htm). It will by itself appear at [research.wur.nl :link:](https://research.wur.nl)

# Differences per chairgroup

Each chairgroup withing the Soil Cluster might have it's own additional way of working, such as: 

- Soil Biology asks the student to deliver the complete dataset in a spreadsheet with clearly defined sheets. Note that one of those sheets is easily copy-and-pasted into the README.txt

- Soil Geography and Landscape provides students by default access to the W:drive, and assumes that they make backups themselves. There are also older instructions around how to create a data management plan for students at dmp.wur.nl. This is another way of getting the same end result: a valid DMP, and hopefully proper data management.