**Data Management Plan: start as Soil Science Cluster Master thesis student**

[Back to home](README.md)

[[_TOC_]]

# Do you understand why you have to write a DMP?

-   to **reduce data loss**;
-   **keep track of your research progress** more efficiently;
-   prepare your research data for **future use**, by you or by others
-   comply with ethical guidelines and institutional requirements (such as the oligation to be able to reproduce your MSc thesis 10 years after completion, in case there are doubts about it's scientific quality).
-   practice proper academic behaviour
-   and to **save a lot of time** in the end

And yes, also because it is obligatory, just as your research proposal.

Please note that I, your [Soil Science Cluster datasteward :link:](https://www.wur.nl/en/persons/luc-steinbuch-1.htm), am around to support and facilite you. You can always reach out to me!

Typically, you might spend 2-4 hours on creating and finetuning your master thesis Data Management Plan.

# Writing your DMP

1.  You will need **WUR Onedrive for Business on your laptop**; if you don't have it yet, please follow the instructions [in English :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010699#OneDrive) or [in Dutch :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010698#OneDrive).

2.  To make your life as easy as possible, WUR Library developped a [template + explanation :link:](https://zenodo.org/records/12805039) for all WU master thesis students. Please **download the MS-Word document** (`WUR_DMP_MSc_TemplateAndGuidance_vXX.docx`), save it in your WUR Ondrive for Business, perhaps rename it, and **share it with both your data steward and your supervisor** (or supervisors).

3.  **Fill out** the template. Feel free to modify the questions, or add topics, if that fits your research proposal; the Data Managment Plan is to guide you towards proper data management, rather than just a compulsory form for a tickmark. Note that you can click "info", or scroll to the end of the document, for explanations.

4.  If you get **stuck on a question, just ask** your datasteward, for example by email. If your fellow thesis students seem have similar questions, we can also assign a thesis ring to the topic of data management.

5.  When you think you are finished, please **ask both your data setward as well as your supervisor** for feedback.

# During your thesis work

If you find it difficult to motivate yourself to perform proper data management in daily practice, perhaps have a look at [Intrinsic motivation](IntrinsicMotivation.md). And you can always ask your data steward in case you encounter pratical problems!


# Finishing your thesis work

1.  Note that at the end of your thesis work, you should **deliver a dataset with a [readme-file](DOC_thoughts_about_readme.md)** (be aware of the mentioned MSc thesis student template) to your supervisor and the data steward, or perhaps you can upload it yourself to the W-drive. In the few cases that it is not explicitly obligatory, we strongly advise to do finish your data activities before the end-colloquium and the examination; afterwards it will be difficult to motivate yourself. 

2. Your supervisor or data steward should register your dataset in [research.wur.nl :link:](https://research.wur.nl) via pure.library AT wur.nl. This means that the existence of the dataset is acknowledged, not that the dataset itself is publicly available. 

3. If your supervisor is in favour, is also possible to publish your dataset; see [Publishing research data – WUR Library supported repositories :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/finishing/publishing-research-data.htm). In that case, it is automatically registered at [research.wur.nl :link:](https://research.wur.nl)


# Differences per chairgroup

Each chairgroup withing the Soil Cluster might have it's own additional way of working, such as 

- Soil Biology asks you to deliver your complete dataset in a spreadsheet with clearly defined sheets. You will get instructions. Note that one of those sheets is easily copy-and-pasted into the README.txt

- Soil Geography and Landscape provides access to the W:drive, and assumes that you make backups yourself. There are also older instructions around how to create a data management plan at dmp.wur.nl; this is another way of getting the same end result: a valid DMP, and hopefully proper data management.



Anyway: :full_moon_with_face: Enjoy your research!
