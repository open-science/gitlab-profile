**Intrinsic motivation**

[Back to home](README.md)

Although I am trained as a scientist in a completely different field, I would like to approach my natural laziness (which you may or may not recognise in yourself) in an as scientific as possible way. I assume (without further proof) that we can use four main perspectives on how to motivate ourselves to get things -- such as proper research data management -- done:


## 1st perspective: Motivators in general

  - Doing **meaningful** work; symbolic immortality\
  *Thus: recognize, perhaps even write down, how important proper research data management is for the scientific community*
  - Creativity, being unique 
  - Self-determination, individuality\
  *Both above motivators are great for the role as scientist, but perhaps less in your equally important role as magager of your research data*
  - **Recognition**, especially personal\
  *Thus: Sincere compliments are great motivators!*
  - Connection, sense of belonging and trust, long-term commitment, be part of a **greater purpose**\
  *... such as the scientific community :wink: *
  
Motivators can pay out on the long term, and might require action when starting up, which is relatively hard. Motivators can also pop up at a natural moment in the work flow, while working on it: this is relatively easy.

Above motivators are intrinsic; there can however also be external motivators, such as **peer pressure; joy of cooperation; urgency**.

## 2nd perspective: "Our brain wants to save energy"

Here, the focus is on the idea that willpower, focus etc. are depletable resources. Thus...

**... how to make it easy: Save mental enery**

  - For the same activity, **habits** use much less mental energy than doing something for the first time. 

  - **Updating** something feels like less work than starting something new: Filling in a template seems much less work than starting a document from scratch.

  - **Split up big actions** into small concrete steps, and focus on the first step only. 

  - **Observe** what interruptions do to your mental energy and your focus, and adjust your environment accordingly.

  - **Organise smartly**. Let the computer do as much as possible mental and practical work for you; use [SyncBack](https://www.2brightsparks.com/syncback/syncback-hub.html) (also available in WUR Software center) to automate backups, instead of doing it manually.

  - **Create external structure**, such as: planned concrete actions with designated hours in a project proposal; colleguae reviewing your code.
  

  - **Limit number of choices** (max. 3 options at one moment; few choice moments during the day).

## 3rd perspective: "We combine multiple brains in one"

We have a habits en routine system, as well as a dopamine driven quick reward system. Beside that, our *homo sapiens* brain is very good in reflection and planning :blush:, but actually not that good in executing this long term planning :confused: because both the the habits&routine system, as well as the direct reward system, are dominating. So..

**... how to develop new habits?**

  - **Repetition**, Repetition, Repetition
  
  - Consolidate **small, concrete steps**. Example:
      1. At the end of each day, check if all newly created files are at the correct spot. If you note that you forget this, put it in your Outlook agenda as repetitive appointment
      2. (added after 1 month): check daily if all newly created research directories have a README
      3. (added after 2 months): check at the end of the each day if your programming code is understandable as it is. If you would leave unexpectedly for one month, would you easily know how to start again once you are back?
      
  - **If -> Then**\
  **If** I use an external dataset, **then** I directly check its license and add it in the README
      


![Campfire with marshmallows. Picture by Marte Hofsteenge, WUR](WUR_marshmellow_with_campfire.jpg "Marshmallow")

## 4th perspective: Delayed gratification

See [Delayed gratification](https://en.wikipedia.org/wiki/Delayed_gratification), in popular culture known as the [Stanford marshmallow experiment](https://en.wikipedia.org/wiki/Stanford_marshmallow_experiment): people are quite different in how far they can resist short-term temptations on the cost of long term rewards. When applied on FAIR research data management:

| Effort | Delayed gratification |
|---|---|
| Create a logical directory structure, file naming conventions, and use it | **Saving time and stress** when looking for older files and projects; easier cooperation |
| Create and update relevant README's | Easily recall project information; **much less work** when sharing data and/or code |
| Set up (and use) a backup structure | Much less stress in case of :scream:|
| Review/update a project Data Management Plan once a year | **Less work** at the end of the project, because needed changes are already implemented |
| Set up a git* repository for code, and commit with meaningful explanations | Save time when looking for bugs and restoring previous versions; save time compared to manually tracking different versions; **much easier to start collaboration** |
| Prepare a FAIR dataset or codeset | **Save time** when for example review is finished and files are needed again |
| Publish a FAIR dataset or codeset | Gratitude and citations from fellow-scientists; **good feeling about oneself** |


# More tips: 

  - Divide a seemingly big or complex task into smaller and concrete portions, and focus on the first one only :mag:. Such as:

    - ~~I will FAIRize all my data~~ -> Today, I register this specific dataset in PURE (WUR's [Research Information system](https://ris.wur.nl); the metadata is visible in https://research.wur.nl )
  
    - ~~I will create a complete README~~ -> Today, I put a README  template in the data directory and fill in the title 


  - **Negotiate with yourself**: Use FAIR / Data management activities as excuse other-**work-avoiding-behavior** (or as study-avoiding-behaviour if you happen to be a student) :innocent:


  - **Know yourself**: Investigate how you function and what works for you! Self-honesty, self-knowledge and reflection are your big friends here :wink:. 

  - **Be kind to yourself**: For example, it is completely impossible to implement all tips on this webpage at once. Choose one to test, and be happy when it works out for you  :partying_face:.



---

Contentwise based on Module 6b of Quik C, Steinbuch L. 2022. Materials from: Workshop FAIR data and data reuse for Environmental Science Group researchers. 4TU.Centre for Research Data. DOI: [10.4121/21399975](https://doi.org/10.4121/21399975)  

Available under CC-BY-NC; Luc Steinbuch, Wageningen UR, 2024



