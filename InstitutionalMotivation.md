**Institutional motivation**


[Back to home](README.md)

In the ACF, Academic Career Framework, the following mentions (among others) in the [Evaluation Indicators document :lock:](https://intranet.wur.nl/Project/ErkennenWaarderenRecognitionReward/Documents?path=%2FAcademic%20Career%20Framework%20formal%20documents) are referring to  Open Science intentions:

Page 14, related to leadership:

![](Institutional_Motivation_ACF_page_14.png)


Page 16 and 18, related to research output:

![](Institutional_Motivation_ACF_page_16.png)

![](Institutional_Motivation_ACF_page_18.png)


Page 35, related to education:

![](Institutional_Motivation_ACF_page_35.png)


Note also that a personal profile with several datasets at [research.wur.nl :link:](https://research.wur.nl) (or other scientific overviews) are more convincing than just peer-reviewed papers.


And, to support motivation in the bigger picture: "Direct research benefits, career benefits and rewards are strong motivators. Also the data sharing norms within a research group, community or discipline are influential. Data sharing policies, infrastructure and data services can provide a framework to support researchers with data management and thereby contribute towards good research data management practices." (Eynden, 2018)


Eynden, Veerle Van den. "3. What Motivates Researchers to Manage and Share Research Data". Research Data Management - A European Perspective, edited by Jesper Boserup Thestrup and Fillip Kruse, Berlin, Boston: De Gruyter Saur, 2018, pp. 43-52. [10.1515/9783110365634-004 :link:](https://doi.org/10.1515/9783110365634-004)

---
