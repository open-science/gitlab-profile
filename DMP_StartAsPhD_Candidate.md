**Data Management Plan: start as Soil Science Cluster PhD candidate**

[Back to home](README.md)

[[_TOC_]]




# Do you understand why you have to write a DMP?

  - to **reduce data loss**;
  - **keep track of your research progress** more efficiently;
  - prepare your research data for **future use**, by you or by others
  - comply with ethical guidelines and institutional requirements
  - practice proper academic behaviour
  - and to **save a lot of time** in the end
  
Please note that I, your [Soil Science Cluster datasteward](https://www.wur.nl/en/persons/luc-steinbuch-1.htm), am around to support and facilite you. You can always reach out to me, and we will probably have at least one meeting anyway, just to see how the writing of your DMP is going.

Anyway, in the context of a PhD project, **a data management plan is a living document meant to guide you**, to help you and to create awareness about proper data management. It is in no way meant to limit you, so don't be afraid to fill in "wrong" information because you don't know yet how your project will evolve. However, you DMP should not be considered as just an obligatory tickmark which you can forget once submitted: during and at the end of your PhD, proper data management is important!

Typically, you might spend 6-12 hours on creating your Data Management Plan, including a meeting with me (your data steward) and eventual a meeting with a security or privacy officer.
  
# Start writing your DMP

For practical reasons, in the Soil Science Cluster we urge you to **use the online system** instead of having a Word document. This online DMP can very conveniently be shared with your supervisors, your data steward, eventual with research partners and with the Wageningen Data Competence Center.

  1. Go to [dmp.wur.nl](https://dmp.wur.nl).  If this is the first time you use it, you have to make an account and connect it to your WUR institutional credentials.  Do not use your WUR password for the DMP account. Note that the whole procedure is quite well explained at (https://zenodo.org/record/7073741). For the rest of your academic career at WUR, you can smoothly use your institutional (=WUR) credentials to get access.
  2. Click “Create plan” or “create plans” (=the same)
  3. Fill in the name of your research thesis, don’t change the research organisation (which should be “Wageningen University and Research”)  and **tick ”no funder”; thus you will end up with the right template** to fill in (also if your research is funded by organisations like NWO). 
  4. Click “create plan” 
  
Overview of your options:

![DMP online overview](DMP_Overview_dmponline.png "")
  

The main work is under "Write plan" tab. There is a **quite extended explanation** at the right part of the screen. In case of doubt or lacking knowledge, you can leave parts empty and discuss with your data steward or supervisors, and when needed with privacy- and security specialists. 
  
## Specific remarks (refers to template v08-03):

### A.6,7,8 List the individuals responsible f...
In most cases, it will be you and your daily supervisors. "Data stewardship / support" can you your Soil Science Cluster [datasteward](https://www.wur.nl/en/persons/luc-steinbuch-1.htm). 

### C.13 Where will the data and accompanying docu... 
As PhD candidate, you get by default access to a part on the W: ("W:drive Massive File Storage Disaster Recovery (WUR network drive)") where you, your supervisors and your data-steward have access; the path will be something like:

  - W:\\ESG\\DOW_SBL\\Research\\\<your name\>
  - W:\\ESG\\DOW_SLM\\PhD_projects\\\<your name\>
  - W:\\ESG\\DOW_SGL\\Research_PhD\\\<your name\>
  - W:\\ESG\\DOW_SOC\\Research_PhD_candidates\\\<your name\>  

If you don't have this folder already, ask your data steward or your secretary. Probably, this W:-drive will be used for archiving after you finish your PhD; during your PhD, it is hightly recommended that you use it directly or use it as backup facility. 

### F Working with sensitive data
We have an ESG Information Security Officer and two ESG Privacy Officers, all of which are easily approachable and willing to help you if you have questions or doubts regarding sensitive data and its eventual classification. Please follow the given links. Better save than sorry!

### F 21: Is this project registered in SmartPIA?
SmartPIA is the WUR internal system which provides overview of privacy-related data in case of a data-breach; such a system is a legal obligation. As this system is currently not working smootly, your supervisor is probably not using it. Ask the ESG Privacy Officers if you should use SmartPIA (and if yes: how?).



:full_moon_with_face: Enjoy your research!

# Useful links

  - [DMP Online](https://dmp.wur.nl); because the first time login might be a bit complicated, see [how to get access to DMP online](https://zenodo.org/records/10572026).
  - [RDM course from WUR library](https://www.wur.nl/en/library/researchers/courses-and-demos-staff/data-management-and-publishing.htm) for PhD's << recommended!   
  - More about [privacy and data management](https://intranet.wur.nl/umbraco/en/about-wur/policy-regulations/privacy-personal-data/) and [data security](https://intranet.wur.nl/umbraco/en/practical-information/information-security/data-classification/).
  - [ORCID](https://orcid.org/): your personal science ID, for the rest of your academic career.
  - About general [data management](README.md) in the Soil Science Cluster.
