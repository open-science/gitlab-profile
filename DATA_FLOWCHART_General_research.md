**Data management obligations for general research project**

[Back to home](README.md)

![Student thesis](DATA_FLOWCHART_General_research.png "")

[Publish data](DATA_publish_dataset.md) 

[Register dataset](PURE_why_how_unpublished_dataset.md)



[Source file (Microsoft Visio)](DATA_FLOWCHART_General_research.vsxd)