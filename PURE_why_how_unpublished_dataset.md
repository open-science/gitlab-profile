**Why -- and how to -- register a non-public dataset?**

[Back to home](README.md)

To be explicit: registering a dataset in PURE means that **it's existence and storage location is noted, not that the dataset itself is uploaded**. Note that publishing the dataset has the preference and will for some grants be required anyway. But assuming you have whatever reason NOT to publish your dataset:

### Why register a not-published dataset?

- It greatly **improves your scientific portfolio**.
- It shows (or at least: **suggests) that your research is solidly backed** up with a dataset, which can be shared in case any doubts arise.
- It helps you **putting some effort in nicely structuring, storing and documenting** the dataset, which will also save you time on the longer term.
- In the unlikely, unwelcome but not impossible event that something serious happens to you, your **colleagues at least know where to find the dataset**.  
- A data availability statement "The data is available upon request" is a bit sloppy anyway. Having the dataset registered makes it a bit less sloppy. 
- Last but not least: although there is no sanction on not doing this yet, **it is just obligatory**: *"The WUR data policy stipulates that all data underlying (a) publication(s) need to be registered in Pure.*" [WUR website, accessed 2025-01-24 :link:](https://www.wur.nl/nl/en/waardecreatie-samenwerking/wdcc-2/how-to-register-and-link-research-data.htm)

### So, how to register a not-published dataset?


1. Collect the following information: 
 * Name of dataset
 * Location of dataset
 * Creators etc (if possible with ORCID)
 * Contact person
 * Related publication or publications
 * Terms of use, license.
2. Create a frozen and well-[documented](DOC_thoughts_about_readme.md) version of your dataset. This should also contain the information collected in the previous point.  
3. Put this frozen dataset at a logical place, such as the W:drive. Other options are the WUR or SURF tape archive, or YODA. 
4. Send all information, with a copy of the README.txt (or something similar), to your data steward, or to data AT wur.nl. 

Thanks! Your future self (among others) will be grateful for your actions!

