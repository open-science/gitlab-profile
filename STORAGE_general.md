**Research (and non-research) Data Storage General overview**

[Back to home](README.md)

[[_TOC_]]

A very accessible but also very extended overview for data storage during research, and eventual archiving after reserach. is given at [Storage finder :link:](https://library.wur.nl/storagefinder/). But in short, the two most important options are:


## [WUR Onedrive for business :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010173)

Standard on all WUR laptops and desktops; quite safe, easy sharing with other people within WUR (including simultaniously working at the same MS office documents), relatively easy [restoring of previous versions or deleted files :link:](https://support.wur.nl/esc?id=kb_article_view&sys_kb_id=b5fad8edc3534ad0ce1098a4e40131b7#onedrive). Can be [added to your own device :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010699#OneDrive), for example for students. Is deleted once you leave WUR.


## [W-drive :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010136)

The location of choice for long-term archiving. As PhD, and sometimes as master thesis student, you will get a folder of the W: drive assigned by your group's secretary, where only you and your supervisor have access. It is owned by your group, so unlike Onedrive, the data is not deleted when you leave WUR. For your Data Management Plan: You will probably have a "Massive File Storage Disaster Recovery" version (and which version it is is a choice by your group anyway). The W:-drive is directly accessible when with a WUR laptop or desktop on-campus. When off-campus or having your own device, you need to [use a VPN connection :link:](https://support.wur.nl/esc?id=kb_article_view&sysparm_article=KB0010661). See also how to 
[restore older or deleted files from W: :link:](https://support.wur.nl/esc?id=kb_article_view&sys_kb_id=b5fad8edc3534ad0ce1098a4e40131b7#networkshare). 

## Other data storage options

  - Similar to the W: drive, you have a **personal M-drive**. This one cannot be shared, and will be deleted once you leave WUR.

  - For archiving big datasets you don't need that often: [Tape Storage :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0013638). This cheaper and environmentally less worse! Getting access to this service is however a bit complicated.
  
  - [GIT* :link:](https://en.wikipedia.org/wiki/Git) is very convenient if you do a lot of coding, because you can store and recal every change you made; it also allows coorpation on code level. It can accomodate small datasets as well. Our own [git.wur.nl :link:](https://www.wur.nl/en/value-creation-cooperation/partnerships-collaborations/wdcc-2/research-data-management-wdcc/doing/manage-your-code-with-gitwur.htm) should be the your first choice, also because you can choose whom to share your code with. [Installing, connecting to the online cloud](GIT_SetupForCodingResearchers.md), and [using GIT*](GIT_BasicsForCodingResearchers.md) has however a steep learning curve.
  
  - [Sharepoint :link:](https://support.wur.nl/esc?id=kb_article&sysparm_article=KB0010085) the same system as the file sytem which comes with MS-Teams, and very related to Onedrive.
  
  - [YODA](YODA_Basics.md)  is a platform for research data storage and versioning, strongly inviting to proper structuring and added metadata, developped at Utrecht University and supported by [SURF :link:](https://www.surf.nl/en/about). In technical sense, it is a skin around [IRODS :link:](https://irods.org/). Is has a webinterface and allows direct file access via  installed or adjusted programs on your computer ("clients"). However, it is currently not so intuitive to use.
  
## And note that, after research,

  - Whenever possible, [publish your dataset](DATA_publish_dataset.md) rather than archive it internally.

  - After archiving a dataset, [you schould register it](PURE_why_how_unpublished_dataset.md)