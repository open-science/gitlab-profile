Data belonging with MSc thesis "<< the name of your thesis >>"

## Related MSc thesis
"<< the name of your thesis >>", << your name >>, << the chairgroup >>,  Wageningen University, << Month and year of finishing thesis>>.  Supervisor: << name supervisor >> 

## Topic summary
<< Copy-and- paste from the abstract of your thesis >>

## Folderstructure << This is just a suggestion, there are of course many many possibilities! You can stay close to the structure you are already using now >>
- RawData  
-- LabResults
-- FieldObservations
-- ImportedCovariates
- ProcessedData
-- Tables
-- Figures
-- Maps
- Code
- Admin
-- Report
-- ResearchProposal
-- Literature
-- Meetings

## Files; software
<< Which kind of files are there; which software was used? If you wrote your own code: did you need to install any special libraries? >>

## Origin of re-used data
<< In case you downloaded data, or got data from your supervisor: explain were it comes from. Perhaps there is a table in your thesis with all this information? In that case, just refer to this table >>

## Codebook
<< Meaning of data: units, geographical projection, way of aquiring, used laboratury analysis etc. If this is clearly described in the "Methods" part of your thesis, you can refer to this section >>

## Used workflow
<< 'With that datafile, do this, to arrive at that datafile and those figures and tables'. Probably a bit more hands-on than in your "Methods" section>>

## License; terms of use
This data set is internally archived at WUR for reproducibility purposes, and as such not meant to be made public or to be shared. 
However, the files << your own observations, and / or your own code >> can be re-used under CC-BY license << if you want this; "CC-BY" means that others can use it but should cite you as the author >>
