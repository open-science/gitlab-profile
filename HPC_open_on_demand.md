**Open on demand: RStudio on HPC**

[Back to home](README.md)

The High Performance Computing Cluster of WUR, also called [Anunna](https://search.researchequipment.wur.nl/SearchDetail.aspx?deviceid=8d495133-99ee-4b81-b059-bbef6d6734b8), 
offers researchers (including PhD's and Master thesis students) the possibility to run calculation and data intensive tasks, among others to execute Python and R code. To facilitate users not skilled in Linux and command line options, the IT department experiments with an user-friendly interface. 
With this "Open on demand" user interface, one can start up R studio with an assigned number of processing cores,working memory 
and an estimated minimum of calculation time. For reasonable use, there are no extra costs involved inside the ESG science group (which is true for all soil cluster chairgroups).

*Please note: at the moment of writing (February 17, 2025), only the slightly outdated R 4.2.1 is supported.*

Steps to use On Demand at WUR:

1. [Ask for access to the HPC :link:](https://support.wur.nl/esc?id=sc_cat_item&table=sc_cat_item&sys_id=e52c5a188776a150b0bd33fd3fbb3536). Note that for Master Thesis Students, their supervisor have to ask for access.

2. Go to [https://ood.anunna.wur.nl/pun/sys/dashboard/batch_connect/sessions :link:](https://ood.anunna.wur.nl/pun/sys/dashboard/batch_connect/sessions). For login, use your WUR email address (so: not your user name).

3. We will focus here on R users using RStudio. (Another option is Python in combination with Jupyter.) Choose RStudio, set the right options, and "Launch" ![Screenshot starting up HPC Open On Demand](HPC_Openondemand_01.png "")

Try to ask for reasonable resources which you are actually going to use. 

4. "Launch" means you request access: the system will look when the requested resources are available (on the background, the [SLURM workload manager](https://en.wikipedia.org/wiki/Slurm_Workload_Manager) is active); in practice, after a few minutes, you can open RStudio server version:
![Screenshot HPC Open On Demand waiting](HPC_Openondemand_02.png "")
![Screenshot HPC Open On Demand Accepted](HPC_Openondemand_03.png "")

5. In the webversion of RStudio, make your project working. Note that in the "Files" pane, it is possible to upload and download code and datasets (also as .zip file). For projects with a longer running time, make sure in the script that the results are saved (in case you lose access to RStudio but the code is still running).



